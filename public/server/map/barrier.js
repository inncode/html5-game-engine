let PhysicsRect = require('../components/physics/physics-rect');

class Barrier extends PhysicsRect {
    constructor(width, height, x ,y) {
        super(width, height);

        this.position.x = x;
        this.position.y = y;
        this.mass = 100;
    }
}

module.exports = Barrier;