let Wall = require('../shared/components/wall');

class WallManager {
    constructor() {
        this.walls = {};
    }

    process(socketId, inputs, players) {
        let keysStack = inputs.getKeys();
        if (keysStack.length <= 1) {
            return false;
        }
        let keys = keysStack.pop();

        //east
        if (keys[65]) {
            this.buildWall(socketId, players, 1);
        }
        //west
        if (keys[68]) {
            this.buildWall(socketId, players, 3);
        }
        //north
        if (keys[87]) {
            this.buildWall(socketId, players, 0);
        }
        //south
        if (keys[83]) {
            this.buildWall(socketId, players, 2);
        }

        return true;
    }

    buildWall(socketId, players, direction) {
        let player = players[socketId];

        if (player.brick <= 0) {
            return 0;
        }
        let playerPosition = player.position;

        let newWall = new Wall();
        newWall.spawn(playerPosition, direction);

        if (!this.walls[socketId]) {
            this.walls[socketId] = [];
        }

        let collide = false;
        let self = this;
        Object.keys(this.walls).forEach(function (wallId) {
            self.walls[wallId].forEach((wall) => {
                if (!collide && wall.position.x === newWall.position.x && wall.position.y === newWall.position.y) {
                    collide = true;
                }
            });
        });
        Object.keys(players).forEach(function (index) {
            if (index === socketId || collide || players[index].dead || players[socketId].dead) {
                return;
            }

            let deltaX = players[index].position.x - Math.max(newWall.position.x, Math.min(players[index].position.x, newWall.position.x + newWall.size.x));
            let deltaY = players[index].position.y - Math.max(newWall.position.y, Math.min(players[index].position.y, newWall.position.y + newWall.size.y));

            if (deltaX == 0 && deltaY == 0) {
                collide = true;
            }
        });

        if (!collide) {
            player.brick --;
            this.walls[socketId].push(newWall);
        }
    }
}

module.exports = WallManager;