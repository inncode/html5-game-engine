let Player = require('../shared/components/player');
let CollisionDetection = require('./math/collision-detection');
let MapObstacles = require('./mysql/map-obstacles');
let Options = require('../shared/options');
let Ammo = require('../shared/components/groundItemAmmo');
let GroundItemWall = require('../shared/components/groundItemWall');
let Input = require('./components/input');
let WallManager = require('./wall-manager');
let Bot = require('../shared/components/bot');

class ServerManager {
    constructor() {
        this.players = {};
        this.obstacles = [];
        this.inputs = [];
        this.setObstacles = this.setObstacles.bind(this);

        this.mapObstacles = new MapObstacles();
        this.mapObstacles.obstacles(this.setObstacles);

        this.groundItems = [];
        this.wallManager = new WallManager();
        this.walls = [];
        this.botCount = 2;
        this.addBots();
    }

    spawnPlayer(socketId, playerData) {
        this.players[socketId] = new Player(playerData.id, playerData.username, playerData.kills, playerData.points, playerData.deaths);
        this.respawnPlayer(socketId);
        if (this.botExist && Object.keys(this.players).length - this.botCount > 1) {
            this.removeBots();
        }
    }

    removePlayer(socketId) {
        delete this.players[socketId];
        if (!this.botExist && Object.keys(this.players).length < 2) {
            this.addBots();
        }
    }

    movePlayer(socketId) {
        if (!this.players[socketId]) {
            return;
        }
        if (this.players[socketId].dead && this.players[socketId].respawnTime - Date.now() < 0) {
            this.players[socketId].ammo = 10;
            this.players[socketId].brick = 10;

            this.respawnPlayer(socketId);
        }

        let self = this;
        let previousPositionX = this.players[socketId].position.x;
        let previousPositionY = this.players[socketId].position.y;
        this.players[socketId].move();

        if (this.inputs[socketId] && !this.players[socketId].dead) {
            this.wallManager.process(socketId, this.inputs[socketId], this.players);
        }
        Object.keys(self.players).forEach(function (index) {
            if (index == socketId || self.players[index].dead || self.players[socketId].dead) {
                return;
            }
            if (self.players[socketId] instanceof Bot &&
                !self.players[socketId].targetPosition &&
                !(self.players[index] instanceof Bot)) {
                self.players[socketId].targetPosition = self.players[index].position;
            }

            if (CollisionDetection.circleCollision(self.players[socketId], self.players[index])) {
                self.players[socketId].position.x = previousPositionX;
                self.players[socketId].position.y = previousPositionY;
                self.players[index].velocity.x = (self.players[index].velocity.x + self.players[socketId].velocity.x) / 2;
                self.players[index].velocity.y = (self.players[index].velocity.y + self.players[socketId].velocity.y) / 2;
                self.players[socketId].velocity.x -= (self.players[socketId].velocity.x + self.players[index].velocity.x) / 2;
                self.players[socketId].velocity.y -= (self.players[socketId].velocity.y + self.players[index].velocity.y) / 2;
            }
        });

        this.obstacles.forEach(obstacle => {
            if (obstacle.circleCollision(self.players[socketId])) {
                self.players[socketId].position.x = previousPositionX;
                self.players[socketId].position.y = previousPositionY;
                let deltaX = previousPositionX - Math.max(obstacle.position.x, Math.min(previousPositionX, obstacle.position.x + obstacle.size.x));
                let deltaY = previousPositionY - Math.max(obstacle.position.y, Math.min(previousPositionY, obstacle.position.y + obstacle.size.y));
                let angle = Math.atan2(self.players[socketId].velocity.y, self.players[socketId].velocity.x) * (180 / Math.PI);
                let normalAngle = Math.atan2(deltaY, deltaX) * (180 / Math.PI);
                angle = 2 * normalAngle - 180 - angle;
                let mag = 0.9 * Math.hypot(self.players[socketId].velocity.x, self.players[socketId].velocity.y);

                self.players[socketId].velocity.x = Math.cos(angle * (Math.PI / 180)) * mag;
                self.players[socketId].velocity.y = Math.sin(angle * (Math.PI / 180)) * mag;
            }
        });

        Object.keys(self.wallManager.walls).forEach(function (wallId) {
            self.wallManager.walls[wallId].forEach((wall) => {
                if (CollisionDetection.circleRectCollision(self.players[socketId], wall)) {
                    let deltaX = previousPositionX - Math.max(wall.position.x, Math.min(previousPositionX, wall.position.x + wall.size.x));
                    let deltaY = previousPositionY - Math.max(wall.position.y, Math.min(previousPositionY, wall.position.y + wall.size.y));
                    let angle = Math.atan2(self.players[socketId].velocity.y, self.players[socketId].velocity.x) * (180 / Math.PI);
                    let normalAngle = Math.atan2(deltaY, deltaX) * (180 / Math.PI);
                    angle = 2 * normalAngle - 180 - angle;
                    let mag = 0.6 * Math.hypot(self.players[socketId].velocity.x, self.players[socketId].velocity.y);

                    self.players[socketId].velocity.x = Math.cos(angle * (Math.PI / 180)) * mag;
                    self.players[socketId].velocity.y = Math.sin(angle * (Math.PI / 180)) * mag;
                    let radius = self.players[socketId].radius;
                    if (deltaX == 0 && deltaY ==0) {
                        self.players[socketId].velocity.x = wall.direction.x * -1;
                        self.players[socketId].velocity.y = wall.direction.y * -1;
                    } else if ((Math.abs(deltaX) < radius && deltaY == 0) || (Math.abs(deltaY) < radius && deltaX == 0) || (Math.abs(deltaY) < radius - 4 && Math.abs(deltaX) < radius - 4)) {
                        self.players[socketId].velocity.x = deltaX / 10;
                        self.players[socketId].velocity.y = deltaY / 10;
                    } else {
                        self.players[socketId].position.x = previousPositionX;
                        self.players[socketId].position.y = previousPositionY;
                    }
                }
            });
        });

        self.players[socketId].gun.rotation = self.players[socketId].gun.rotation+=Math.PI/45;

        self.players[socketId].gun.bullets.forEach((bullet, index) => {
           ServerManager.moveBullet(bullet);
            let hitIndex = bullet.checkCollisionWithPlayers(this.players);
            if (hitIndex != null) {
                self.players[socketId].score.kills++;
                self.players[socketId].score.points += 15;

                self.players[hitIndex].score.deaths++;
                self.players[hitIndex].respawnTime = Date.now() + 3000;
                self.players[hitIndex].dead = true;

                self.players[socketId].gun.bullets.splice(index, 1);
            }
            if (bullet.checkCollisionWithObstacles(this.obstacles)) {
                self.players[socketId].gun.bullets.splice(index, 1);
            }
            if (bullet.checkCollisionWithWalls(self.wallManager)) {
                self.players[socketId].gun.bullets.splice(index, 1);
            }
        });

        this.groundItems.forEach((groundItem, index) => {
            if (groundItem.circleCollision(self.players[socketId])) {
                groundItem.pickUp(self.players[socketId]);

               this.groundItems.splice(index, 1);
            }
        });
    }

    static moveBullet(bullet) {
        bullet.position.x += bullet.velocityX;
        bullet.position.y += bullet.velocityY;
    }

    receivedPlayerInput(socketId, keys) {
        if (!this.players[socketId]) {
            return;
        }

        if (!this.inputs[socketId]) {
            this.inputs[socketId] = new Input();
        }

        this.inputs[socketId].addInputToStack(keys);
        this.players[socketId].addInputToStack(keys);
    }

    setObstacles(obstacles) {
        this.obstacles = obstacles;
    }

    respawnPlayer(socketId) {
        this.players[socketId].position.x = Math.floor(Math.random() * Options.getCanvas().width);
        this.players[socketId].position.y = Math.floor(Math.random() * Options.getCanvas().height);

        let collision = false;
        let self = this;
        Object.keys(self.players).forEach(function (index) {
            if (index == socketId || self.players[index].dead || self.players[socketId].dead) {
                return;
            }
            if (!collision && CollisionDetection.circleCollision(self.players[socketId], self.players[index])) {
                collision = true;
            }
        });
        this.obstacles.forEach(obstacle => {
            if (!collision && obstacle.circleCollision(self.players[socketId])) {
                collision = true;
            }
        });

        Object.keys(self.wallManager.walls).forEach(function (wallId) {
            self.wallManager.walls[wallId].forEach((wall) => {
                if (!collision && CollisionDetection.circleRectCollision(self.players[socketId], wall)) {
                    collision = true;
                }
            });
        });

        if (collision) {
            this.respawnPlayer(socketId);
            return;
        }

        this.players[socketId].dead = false;
    }

    spawnGroundItem() {
        if (this.groundItems.length === Options.getGroundItemOptions().limit) {
            this.groundItems.shift();
        }

        let groundItem = ServerManager.getRandomGroundItemType();
        groundItem.position.x = Math.floor(Math.random() * Options.getCanvas().width);
        groundItem.position.y = Math.floor(Math.random() * Options.getCanvas().height);

        let playerCollision = groundItem.checkCollisionWithPlayers(this.players);
        let obstacleCollision = groundItem.checkCollisionWithObstacles(this.obstacles);
        let groundItemCollision = groundItem.checkCollisionWithGroundItems(this.groundItems);
        let wallCollision = groundItem.checkCollisionWithWalls(this.wallManager);

        if (playerCollision || obstacleCollision || groundItemCollision || wallCollision) {
            this.spawnGroundItem();
            return;
        }

        this.groundItems.push(groundItem);
    }

    addBots() {
        for (let i = 1; i <= this.botCount; i++) {
            this.players[i] = new Bot(i, '#'+i+'. BOT', 0, 0, 0);
            this.respawnPlayer(i);
        }
        this.botExist = true;
    }

    removeBots() {
        for (let i = 1; i <= this.botCount; i++) {
            delete this.players[i];
        }
        this.botExist = false;
    }

    static getRandomGroundItemType() {
        let typesArray = ['wall', 'ammo'];
        let type = typesArray[Math.floor(Math.random() * typesArray.length)];

        switch (type) {
            case 'wall':
                return new GroundItemWall();
            case 'ammo':
                return new Ammo();
        }
    }
}

module.exports = ServerManager;