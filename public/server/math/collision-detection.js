class CollisionDetection
{
    static rectCollision(rect1, rect2) {
        return rect1.position.x < rect2.position.x + rect2.size.x &&
            rect1.position.x + rect1.size.x > rect2.position.x &&
            rect1.position.y < rect2.position.y + rect2.size.y &&
            rect1.size.y + rect1.position.y > rect2.position.y;
    }

    static circleCollision(circle1, circle2) {
        let dx = circle2.position.x - circle1.position.x;
        let dy = circle2.position.y - circle1.position.y;
        let sumRadius = circle1.radius + circle2.radius;

        return Math.pow(dx, 2) + Math.pow(dy, 2) < Math.pow(sumRadius, 2);
    }

    static circleRectCollision(circle, rect) {
        let circleX = circle.position.x;
        let circleY = circle.position.y;

        let deltaX = circleX - Math.max(rect.position.x, Math.min(circleX, rect.position.x + rect.size.x));
        let deltaY = circleY - Math.max(rect.position.y, Math.min(circleY, rect.position.y + rect.size.y));

        return Math.pow(deltaX, 2) + Math.pow(deltaY, 2) < Math.pow(circle.radius, 2);
    }
}

module.exports = CollisionDetection;