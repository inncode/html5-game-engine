let Options = require('../../shared/options');
let mysql = require('mysql');

class User {
    checkUser(username, password, callback) {
        this.connection = mysql.createConnection(Options.getDatabase());
        let query = 'SELECT * FROM user WHERE username = "'+username+'" AND password = "'+password+'" AND logged_in = false';
        this.connection.query(query, function (err, result) {
            if (err || result.length === 0) {
                callback({ 'status': false, 'data': {}, 'message': 'Login failed' });
            } else {
                callback({ 'status': true, 'data': result[0], 'message': 'Logged in successfully!' });
            }
        });
        this.connection.end();
    }

    registerUser(username, password, callback) {
        this.connection = mysql.createConnection(Options.getDatabase());
        let query = 'INSERT INTO user (username, password) VALUES ("'+username+'", "'+password+'");';
        this.connection.query(query, function (err, result) {
            if (err) {
                callback({ 'status': false, 'message': 'Username is already taken' });
            } else {
                callback({ 'status': true, 'message': 'User registered successfully!', 'data': {'id': result.insertId }});
            }
        });
        this.connection.end();
    }

    saveScore(player) {
        this.connection = mysql.createConnection(Options.getDatabase());
        let query = 'UPDATE user SET kills = '+player.score.kills+', points = '+player.score.points+', ' +
        'deaths = '+player.score.deaths+' WHERE id = '+player.playerId;
        this.connection.query(query, function (err, result) {
            if (err) {
                console.log(err);
            } else {
                console.log('Player id: '+player.playerId+' saved!');
            }
        });
        this.connection.end();
    }

    getHighscores(callback) {
        this.connection = mysql.createConnection(Options.getDatabase());
        let query = 'SELECT id, username, points, games, deaths FROM user ORDER BY points DESC LIMIT 10';
        this.connection.query(query, function (err, result) {
            if (err) {
                console.log(err);
            } else {
                callback(result);
            }
        });
        this.connection.end();
    }

    login(playerId, value) {
        this.connection = mysql.createConnection(Options.getDatabase());
        let query = 'UPDATE user SET logged_in = '+value+' WHERE id = '+playerId;
        this.connection.query(query, function (err, result) {
            if (err) {
                console.log(err);
            } else {
                console.log('Player id: '+playerId+' logged in!');
            }
        });
        this.connection.end();
    }

    logOutAll() {
        this.connection = mysql.createConnection(Options.getDatabase());
        this.connection.query('UPDATE user SET logged_in = false');
        this.connection.end();
    }
}

module.exports = User;