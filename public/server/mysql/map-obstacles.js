let Barrier = require('../map/barrier');
let Options = require('../../shared/options');
let mysql = require('mysql');


class MapObstacles {
    obstacles(callback) {
        this.connection = mysql.createConnection(Options.getDatabase());

        let query = 'SELECT * FROM map_obstacles';
        let obstacles = [];
        this.connection.query(query, function (err, result) {
            if (err) {
                return [];
            }

            result.forEach(function (val) {
                obstacles.push(new Barrier(val.width, val.height, val.position_x, val.position_y));
            });

            return callback(obstacles);
        });

        this.connection.end();
    }
}

module.exports = MapObstacles;