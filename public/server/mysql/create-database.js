let mysql = require('mysql');
let Options = require('../../shared/options');

let con = mysql.createConnection(Options.getDatabase());

let values = [
    [100, 100, 100, 100],
    [400, 200, 100, 50],
    [600, 250, 50, 150],
    [0, -5, Options.getCanvas().width, 5],
    [-5, 0, 5, Options.getCanvas().height],
    [Options.getCanvas().width, 0, 5, Options.getCanvas().height],
    [0, Options.getCanvas().height, Options.getCanvas().width, 5],
];

// con.query("CREATE DATABASE "+Options.getDatabase().database, function (err, result) {
//     // if (err) { throw err; }
//     console.log("Database created");
// });

let sql = "CREATE TABLE map_obstacles (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, position_x double NOT NULL, position_y double NOT NULL, width double NOT NULL, height double NOT NULL, enabled boolean NOT NULL DEFAULT TRUE)";
con.query(sql, function (err, result) {
    if (err) { throw err; }
});
console.log("Table 'map_obstacles' created");

sql = "CREATE TABLE user (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, username varchar(255) NOT NULL UNIQUE, password varchar(255) NOT NULL, kills int DEFAULT 0, points int DEFAULT 0, games int DEFAULT 0, deaths int DEFAULT 0, logged_in BOOLEAN DEFAULT FALSE)";
con.query(sql, function (err, result) {
    if (err) { throw err; }
});
console.log("Table 'user' created");

let sqlInsert = "INSERT INTO map_obstacles (position_x, position_y, width, height) values ?";
con.query(sqlInsert, [values]);

con.end();
