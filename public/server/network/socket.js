let User = require('../mysql/user');
let io = require('socket.io');
let Options = require('../../shared/options');
let Bot = require('../../shared/components/bot');

class Socket {

    constructor(httpServer, serverManager) {
        this.socket = io.listen(httpServer);
        this.serverManager = serverManager;

        this.setSocketHandler();

        this.fpsCounter = 0;
        this.lobby = {};
    }

    setSocketHandler() {
        this.socket.sockets.on('connection', this.onConnection.bind(this));
    }

    onConnection(socket) {
        let self = this;
        this.socketId = socket.id;
        console.log('connected: ' + this.socketId);

        socket.on('login', function (data) {
            self.login(socket, data);
        });

        socket.on('register', function (data) {
            let userData = data;
            if (userData.username !== '' && userData.password !== '') {
                let user = new User();
                user.registerUser(userData.username, userData.password, function (data) {
                    if (data.status) {
                        self.login(socket, userData);
                    } else {
                        socket.emit('login_register_response', data);
                    }
                });
            } else {
                socket.emit('login_register_response', {'status': false, 'message': 'All fields are required'});
            }
        });

        socket.on('disconnect', function () {
            self.onDisconnect(socket);
        });

        socket.on('new_player', function (data) {
            socket.emit("sync_obstacles", self.serverManager.obstacles);
            self.onNewPlayer(socket, data);
        });

        socket.on('input', function (data) {
            self.onInput(socket, data);
        });

        socket.on('highscores', function () {
            let user = new User();
            user.getHighscores(function (data) {
                socket.emit('highscores-response', data);
            });
        });

        socket.on('latency-server', function() {
            socket.emit('latency-client');
        });
    }

    onDisconnect(socket) {
        console.log('disconnect: ' + socket.id);
        let user = new User();
        if (this.lobby[socket.id]) {
            user.login(this.lobby[socket.id], false);
            delete this.lobby[socket.id];
        }
        if (this.serverManager.players[socket.id] == null) {
            return;
        }

        user.saveScore(this.serverManager.players[socket.id]);
        this.serverManager.removePlayer(socket.id);
    }

    onNewPlayer(socket, data) {
        console.log('Spawned new player');
        this.serverManager.spawnPlayer(socket.id, data.data);
    }

    onInput(socket, keys) {
        this.serverManager.receivedPlayerInput(socket.id, keys);
    }

    updateAll() {
        let self = this;
        let players = {};
        Object.keys(this.serverManager.players).forEach(function (val) {
            players[val] = (({ position, gun, username, score, dead, ammo, brick }) =>
                ({ position, gun, username, score, dead, ammo, brick }))(self.serverManager.players[val]);
        });
        Object.keys(this.serverManager.players).forEach(function (val) {
            self.serverManager.movePlayer(val);
            self.socket.to(val).emit('state', players);
            self.socket.to(val).emit('update-walls', self.serverManager.wallManager.walls);
        });

        this.serverManager.groundItems.forEach((groundItem, index) => {
            groundItem.rotation += Math.PI / 180;
        });

        this.fpsCounter++;
        if (this.fpsCounter >= Options.getGroundItemOptions().fpsRate) {
            self.serverManager.spawnGroundItem();
            this.fpsCounter = 0;
        }

        self.socket.emit('sync_ground_items', self.serverManager.groundItems);
    }

    login(socket, data) {
        if (data.username !== '' && data.password !== '') {
            let user = new User();
            let self = this;
            user.checkUser(data.username, data.password, function (data) {
                if (data.status) {
                    user.login(data.data.id, true);
                    self.lobby[socket.id] = data.data.id;
                }
                socket.emit('login_register_response', data);
            });
        } else {
            socket.emit('login_register_response', {'status': false, 'message': 'All fields are required'});
        }
    }
}

module.exports = Socket;