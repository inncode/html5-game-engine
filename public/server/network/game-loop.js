class GameLoop {
    constructor(socket) {
        /**
         Length of a tick in milliseconds. The denominator is your desired framerate.
         e.g. 1000 / 20 = 20 fps,  1000 / 60 = 60 fps
         */
        this.tickLengthMs = 1000 / 60;
        this.previousTick = Date.now();
        this.actualTicks = 0;
        this.socket = socket;
        this.run = this.run.bind(this);
    }

    run() {
        let now = Date.now();
        this.actualTicks++;
        if (this.previousTick + this.tickLengthMs <= now) {
            let delta = (now - this.previousTick) / 1000;
            this.previousTick = now;

            this.update(delta);

            // console.log('delta', delta, '(target: ' + this.tickLengthMs + ' ms)', 'node ticks', this.actualTicks);
            this.actualTicks = 0;
        }

        if (Date.now() - this.previousTick <= this.tickLengthMs) {
            setTimeout(this.run, this.tickLengthMs - (Date.now() - this.previousTick));
        } else {
            setImmediate(this.run);
        }
    }

    update(delta) {
        this.socket.updateAll();
    }
}

module.exports = GameLoop;