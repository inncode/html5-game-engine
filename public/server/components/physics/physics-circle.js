let Circle = require('../../../shared/components/circle');
let CollisionDetection = require('../../math/collision-detection');

class PhysicsCircle extends Circle {
    constructor(radius)
    {
        super(radius);
    }

    circleCollision(circle) {
        return CollisionDetection.circleCollision(circle, this);
    }

    rectCollision(rect) {
        return CollisionDetection.circleRectCollision(this, rect);
    }
}

module.exports = PhysicsCircle;