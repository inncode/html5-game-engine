let Rect = require('../../../shared/components/rect');
let CollisionDetection = require('../../math/collision-detection');

class PhysicsRect extends Rect {
    circleCollision(circle) {
        return CollisionDetection.circleRectCollision(circle, this);
    }

    rectCollision(rect) {
        return CollisionDetection.rectCollision(rect, this);
    }
}

module.exports = PhysicsRect;