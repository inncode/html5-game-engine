class Input {
    constructor() {
        this.keys = [];
    }

    addInputToStack(keys) {
        this.keys.push(keys);
    }

    getKeys() {
        /**
         * TODO Fix it! Terrible hack. The problem is that keys array keeps growing and eventually crashes server.
         */
        if (this.keys.length > 5) {
            this.keys.splice(0, (this.keys.length - 4));
        }

        return this.keys;
    }
}

module.exports = Input;
