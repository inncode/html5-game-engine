let Vec = require('./vec');

class Circle
{
    constructor(radius = 10) {
        this.position = new Vec();
        this.radius = radius;
    }
}

module.exports = Circle;