let GroundItem = require('./groundItem');

class GroundItemWall extends GroundItem
{
    constructor(amount = 5)
    {
        super(amount);
        this.imgSrc = '../../client/img/ground-item-wall.png';
        this.name = this.constructor.name;
    }

    pickUp(player)
    {
        player.brick += this.amount;
    }
}

module.exports = GroundItemWall;