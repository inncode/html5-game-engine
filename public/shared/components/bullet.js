let Vec = require('./vec');
let CollisionDetection = require('../../server/math/collision-detection');
let Circle = require('./circle');

class Bullet extends Circle
{
    constructor(x, y, rotation, radius = 5, speed = 6) {
        super(radius);

        this.position = new Vec(x, y);

        this.active = true;
        this.rotation = rotation;
        this.speed = speed;
        this.velocityX = Math.cos(this.rotation) * this.speed;
        this.velocityY = Math.sin(this.rotation) * this.speed;
    }

    draw(context) {
        context.beginPath();
        context.fillStyle = 'black';
        context.arc(this.position.x, this.position.y, this.radius, 0, 2*Math.PI);
        context.fill();
    }

    checkCollisionWithPlayers(players) {
        let self = this;
        let hit = null;

        Object.keys(players).forEach(function (index) {
            if (!players[index].dead && !hit && CollisionDetection.circleCollision(players[index], self)) {
                /**
                 * Collides with alive player
                 */

                hit = index;
            }
        });

        return hit;
    }

    checkCollisionWithObstacles(obstacles) {
        let self = this;

        for (var i = 0; i < obstacles.length; i++) {
            if (obstacles[i].circleCollision(self)) {
                /**
                 * Collides with obstacle
                 */
                return true;
            }
        }

        return false;
    }

    checkCollisionWithWalls(wallManager) {
        let self = this;
        let hit = false;

        Object.keys(wallManager.walls).forEach(function (socketId) {
            wallManager.walls[socketId].forEach((wall, key) => {
                if (!hit && CollisionDetection.circleRectCollision(self, wall)) {
                    /**
                     * Collides with alive player
                     */
                    wallManager.walls[socketId].splice(key, 1);
                    hit = true;
                }
            });
        });

        return hit;
    }
}

module.exports = Bullet;