let Vec = require('./vec');
let Circle = require('./circle');
let Options = require('../options');
let Gun = require('./gun');

class Player extends Circle
{
    constructor(id, username, kills, points, deaths)
    {
        super(24);
        this.speed = 3;
        this.friction = 0.98;
        this.velocity = new Vec();

        this.score = 0;

        this._lastPos = new Vec();

        this.position = new Vec();
        this.keys = [];

        this.gun = new Gun();
        this.fpsCounter = 0;
        this.fireRate = 0.25;
        this.ammo = 10;
        this.brick = 10;

        this.playerId = id;
        this.username = username;
        this.score = {
            'kills': kills,
            'points': points,
            'deaths': deaths,
        };

        this.dead = false;
    }

    move() {
        this.fpsCounter++;
        if (this.dead) {
            return;
        }
        let keys = this.getInputFromStack();
        this.moveDirection(keys);
        this.processShot(keys);

        // apply some friction to y velocity.
        this.velocity.y *= this.friction;
        this.position.y += this.velocity.y;

        // apply some friction to x velocity.
        this.velocity.x *= this.friction;
        this.position.x += this.velocity.x;

        //Check for map boundaries
        if (this.position.y <= this.radius) {
            this.position.y = this.radius;
        } else if(this.position.y >= Options.getCanvas().height - this.radius) {
            this.position.y = Options.getCanvas().height - this.radius;
        }

        if (this.position.x <= this.radius) {
            this.position.x = this.radius;
        } else if(this.position.x >= Options.getCanvas().width - this.radius) {
            this.position.x = Options.getCanvas().width - this.radius;
        }
    }

    addInputToStack(keys) {
        this.keys.push(keys);
    }

    getInputFromStack() {
        if (this.keys.length === 0) {
            return [];
        }
        /**
         * TODO Fix it! Terrible hack. The problem is that keys array keeps growing and eventually crashes server.
         */
        if (this.keys.length > 5) {
            this.keys.splice(0, (this.keys.length - 2));
        }

        return this.keys.pop();
    }

    moveDirection(keys) {
        if (keys[38]) {
            if (this.velocity.y > -this.speed) {
                this.velocity.y--;
            }
        }
        if (keys[40]) {
            if (this.velocity.y < this.speed) {
                this.velocity.y++;
            }
        }
        if (keys[39]) {
            if (this.velocity.x < this.speed) {
                this.velocity.x++;
            }
        }
        if (keys[37]) {
            if (this.velocity.x > -this.speed) {
                this.velocity.x--;
            }
        }
    }

    processShot(keys) {
        if (keys[32] && Date.now() > this.gun.lastShot + this.fireRate * 1000  && this.ammo > 0) {
            this.gun.shoot(this.position.x, this.position.y);
            this.fpsCounter = 0;
            this.ammo--;
        }
    }
}

module.exports = Player;