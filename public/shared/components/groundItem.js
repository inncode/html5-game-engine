let Vec = require('./vec');
let PhysicsCircle = require('../../server/components/physics/physics-circle');
let CollisionDetection = require('../../server/math/collision-detection');

class GroundItem extends PhysicsCircle
{
    constructor(amount)
    {
        super(25);
        this.position = new Vec();
        this.amount = amount;

        this.rotation = 0;
    }

    draw(context) {
        context.save();

        this.image = new Image();
        this.image.src = this.imgSrc;

        context.translate(this.position.x, this.position.y);
        context.rotate(this.rotation);
        context.drawImage(this.image, -this.image.width/2, -this.image.height/2);

        context.restore();
    }

    checkCollisionWithPlayers(players) {
        let self = this;
        Object.keys(players).forEach(function (index) {
            if (!players[index].dead && CollisionDetection.circleCollision(players[index], self)) {
                /**
                 * Collides with alive player
                 */
                return true;
            }
        });

        return false;
    }

    checkCollisionWithGroundItems(groundItems) {
        let self = this;
        for (let i = 0; i < groundItems.length; i++) {
            if (groundItems[i].circleCollision(self)) {
                /**
                 * Collides with another ground item
                 */
                return true;
            }
        }

        return false;
    }

    checkCollisionWithObstacles(obstacles) {
        let self = this;
        for (let i = 0; i < obstacles.length; i++) {
            if (obstacles[i].circleCollision(self)) {
                /**
                 * Collides with obstacle
                 */
                return true;
            }
        }

        return false;
    }

    checkCollisionWithWalls(wallManager) {
        let self = this;
        Object.keys(wallManager.walls).forEach(function (socketId) {
            wallManager.walls[socketId].forEach((wall) => {
                if (CollisionDetection.circleRectCollision(self, wall)) {
                    /**
                     * Collides with wall
                     */
                    return true;
                }
            });
        });

        return false;
    }
}

module.exports = GroundItem;