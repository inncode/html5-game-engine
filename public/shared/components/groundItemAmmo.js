let GroundItem = require('./groundItem');

class GroundItemAmmo extends GroundItem
{
    constructor(amount = 10)
    {
        super(amount);
        this.imgSrc = '../../client/img/ground-item-ammo.png';
        this.name = this.constructor.name;
    }

    pickUp(player)
    {
        player.ammo += this.amount;
    }
}

module.exports = GroundItemAmmo;