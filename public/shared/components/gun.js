let Rect = require('./rect');
let Vec = require('./vec');
let Bullet = require('./bullet');

class Gun extends Rect
{
    constructor(width = 10, height = 15) {
        super(width, height);

        this.rotation = 0;
        this.bullets = [];
        this.lastShot = Date.now();
    }

    draw(context, x, y, isLocalPlayer) {
        context.save();

        context.translate(x, y);
        context.rotate(this.rotation);

        if (isLocalPlayer) {
            context.fillStyle = '#A4D3A0';
        } else {
            context.fillStyle = '#D19C9D';
        }

        context.fillRect(-this.size.x/2+25, -this.size.y/2, this.size.x, this.size.y);

        context.restore();
    }

    shoot(x, y) {
        this.lastShot = Date.now();
        this.bullets.push(new Bullet(Math.cos(this.rotation) * 30 + x, Math.sin(this.rotation) * 30 + y, this.rotation));
    }

    get velocity() {
        return new Vec(Math.cos(this.rotation) * 30, Math.sin(this.rotation) * 30);
    }
}

module.exports = Gun;