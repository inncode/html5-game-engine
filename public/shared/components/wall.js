let Rect = require('./rect');
let Options = require('../options');
let Vec = require('./vec');

class Wall extends Rect
{
    constructor() {
        super(Options.getCanvas().gridSize, Options.getCanvas().gridSize);
        this.position = new Vec();
        this.direction = new Vec();
    }

    //direction 0 - north, 1 - west, 2 - south, 3 - east
    spawn(playerPosition, direction) {
        let gridX = Math.floor(playerPosition.x / Options.getCanvas().gridSize);
        let gridY = Math.floor(playerPosition.y / Options.getCanvas().gridSize);

        switch (direction) {
            case 0:
                gridY --;
                this.direction.y = -2;
                break;
            case 1:
                gridX --;
                this.direction.x = -2;
                break;
            case 2:
                gridY ++;
                this.direction.y = 2;
                break;
            case 3:
                gridX ++;
                this.direction.x = 2;
                break;
        }

        this.position.x = gridX * Options.getCanvas().gridSize;
        this.position.y = gridY * Options.getCanvas().gridSize;
    }
}

module.exports = Wall;