let Player = require('./player');

class Bot extends Player {
    constructor(id, username, kills, points, deaths) {
        super(id, username, kills, points, deaths);
        this.target = null;
    }

    moveDirection(keys) {
        if (!this.targetPosition) {
            return;
        }
        if (this.targetPosition.y + 150 < this.position.y) {
            if (this.velocity.y > -this.speed) {
                this.velocity.y--;
            }
        }
        if (this.targetPosition.y - 150 > this.position.y) {
            if (this.velocity.y < this.speed) {
                this.velocity.y++;
            }
        }
        if (this.targetPosition.x - 150 > this.position.x) {
            if (this.velocity.x < this.speed) {
                this.velocity.x++;
            }
        }
        if (this.targetPosition.x + 150 < this.position.x) {
            if (this.velocity.x > -this.speed) {
                this.velocity.x--;
            }
        }
    }

    processShot(keys) {
        if (!this.targetPosition) {
            return;
        }
        let otherX = this.targetPosition.x;
        let otherY = this.targetPosition.y;
        let deltaX = this.gun.velocity.x + this.position.x;
        let deltaY = this.gun.velocity.y + this.position.y;
        let conditionX = (deltaX - this.position.x) / (otherX - this.position.x);
        let conditionY = (deltaY - this.position.y) / (otherY - this.position.y);

        let tolerance = Math.floor((Math.random() * 10) + 1) / 100;
        if (conditionX - tolerance <= conditionY && conditionX + tolerance >= conditionY &&
            ((this.position.x < deltaX && deltaX < otherX || this.position.x > deltaX && deltaX > otherX) ||
                (this.position.y < deltaY && deltaY < otherY || this.position.y > deltaY && deltaY > otherY)) &&
            Date.now() > this.gun.lastShot + this.fireRate * 1000) {
            this.gun.shoot(this.position.x, this.position.y);
            this.targetPosition = null;
        }
    }

    set targetPosition(targetPosition) {
        this.target = targetPosition;
    }

    get targetPosition() {
        return this.target;
    }
}

module.exports = Bot;