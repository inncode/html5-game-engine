class Vec
{
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }

    add(vec2) {
        return new Vec(this.x + vec2.x, this.y + vec2.y);
    }

    addTo(vec2) {
        this.x += vec2.x;
        this.y += vec2.y;
    }

    subtract(vec2) {
        return new Vec(this.x - vec2.x, this.y - vec2.y);
    }

    subtractFrom(vec2) {
        this.x -= vec2.x;
        this.y -= vec2.y;
    }

    get magnitude() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }
}

module.exports = Vec;