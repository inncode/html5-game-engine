class Options {
    static getCanvas() {
        return {
            width: 1300,
            height: 750,
            gridSize: 50
        };
    }

    static getDatabase() {
        return {
            host: 'db',
            port: '3306',
            user: 'root',
            password: 'root',
            database: 'GameEngine',
        }
    }

    static getGroundItemOptions() {
        return {
            limit: 6,
            fpsRate: 700,
        }
    }
}

module.exports = Options;