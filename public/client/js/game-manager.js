import backGroundImage from '../img/bg.jpg';
import playerImage from '../img/player.png';
import remotePlayerImage from '../img/player-remote.png';
import Player from '../../shared/components/player';
import Barrier from './components/map/barrier';
import Bullet from '../../shared/components/bullet';
import {GameOverlay} from './components/game-overlay';
import GroundItemAmmo from '../../shared/components/groundItemAmmo';
import GroundItemWall from '../../shared/components/groundItemWall';
import Wall from '../../shared/components/wall';
import WallTexture from '../img/texture/wood4.jpg';
import MetalTexture from '../img/texture/metal2.jpg';

export class GameManager {
    constructor(canvas) {
        this._canvas = canvas;
        this._context = canvas.getContext('2d');
        this.gameOverlay = new GameOverlay(this._canvas);

        this.players = [];
        this.obstacles = [];
        this.groundItems = [];
        this.walls = [];

        this.background = null;
        this.localSocketId = null;
        this.latency = 0;
        this.fps = 60;
        let lastTime = null;
        let tickLengthMs = 1000 / 240;
        this._frameCallback = (millis) => {
            if (lastTime == null) {
                lastTime = millis;
            }

            if (millis >= lastTime + tickLengthMs) {
                const diff = millis - lastTime;
                this.update(diff / 1000);
                lastTime = millis;
                this.fps = Math.floor(1000 / diff);
            }
            requestAnimationFrame(this._frameCallback);
        };

        this.updatePlayer = this.updatePlayer.bind(this);
        this.syncObstacles = this.syncObstacles.bind(this);
        this.addWall = this.addWall.bind(this);
    }

    start() {
        requestAnimationFrame(this._frameCallback);
    }

    clear() {
        this._context.save();
        if (this.background) {
            this._context.drawImage(this.background, (screen.width-this._canvas.width)/2, 0, this._canvas.width, this._canvas.height, 0, 0, this._canvas.width, this._canvas.height);
        } else {
            let background = new Image();
            background.src = backGroundImage;
            background.onload = () => {
                this._context.fillStyle = this._context.createPattern(background, 'repeat');
                this._context.fillRect(0, 0, screen.width, screen.height);
            };
            this.background = background;
        }
        this.gameOverlay.drawGrid();
        this._context.restore();
    }

    update(dt) {
        this.draw();
    }

    draw() {
        this.clear();

        this.obstacles.forEach(obstacle => {
            obstacle.draw(this._context, MetalTexture);
        });

        this.walls.forEach(wall => {
            let image = new Image();
            image.src = WallTexture;
            this._context.drawImage(image, wall.position.x, wall.position.y, wall.size.x, wall.size.y);
        });

        this.groundItems.forEach(item => {
            item.draw(this._context);
        });

        Object.entries(this.players).forEach(player => {
            this.drawPlayer(player[1]);
            this.gameOverlay.drawPlayerName(player[1]);
        });

        if (!this.players[this.localSocketId]) {
            return;
        }

        if (this.players[this.localSocketId].respawnTime - Date.now() > 0) {
            this.gameOverlay.drawRespawnTimerOverlay(Math.ceil((this.players[this.localSocketId].respawnTime - Date.now()) / 1000));
        }
    }

    drawPlayer(player) {
        this._context.save();
        if (!player.dead) {
            let playerSprite = new Image();
            playerSprite.src = player.localPlayer ? playerImage : remotePlayerImage;
            this._context.drawImage(playerSprite, player.position.x - player.radius, player.position.y - player.radius);
            player.gun.draw(this._context, player.position.x, player.position.y, player.localPlayer);
        }
        player.gun.bullets.forEach(bullet => {
            bullet.draw(this._context);
        });

        if (player.localPlayer) {
            this.gameOverlay.drawPlayerInfo(player);
            this.gameOverlay.drawPerformanceStats(this.latency, this.fps);
        }

        this._context.restore();
    }

    resetPlayers() {
        this.players = [];
    }

    updatePlayer(socketId, player) {
        if (!this.players[socketId]) {
            this.players[socketId] = new Player();
        }

        this.players[socketId].position.x = player.position.x;
        this.players[socketId].position.y = player.position.y;

        this.players[socketId].username = player.username;
        this.players[socketId].score = player.score;
        this.players[socketId].ammo = player.ammo;
        this.players[socketId].brick = player.brick;
        if (!this.players[socketId].dead && player.dead) {
            this.players[socketId].respawnTime = Date.now() + 3000;
        }
        this.players[socketId].dead = player.dead;

        this.players[socketId].gun.rotation = player.gun.rotation;

        this.syncBullets(player.gun.bullets, this.players[socketId]);

        if (socketId === this.localSocketId) {
            this.players[socketId].localPlayer = true;
        }
    }

    removePlayer(socketId) {
        delete this.players[socketId];
    }

    resetObstacles() {
        this.obstacles = [];
    }

    syncObstacles(data) {
        let obstacle = new Barrier(data.size.x, data.size.y);
        obstacle.position = data.position;

        this.obstacles.push(obstacle);
    }

    syncGroundItems(data) {
        this.groundItems = [];
        data.forEach(item => {
            let groundItem = GameManager.getGroundItemClass(item.name);
            groundItem.position = item.position;
            groundItem.radius = item.radius;
            groundItem.amount = item.amount;
            groundItem.imgSrc = item.imgSrc;
            groundItem.rotation = item.rotation;

            this.groundItems.push(groundItem);
        });
    }

    static getGroundItemClass(name) {
        switch (name) {
            case 'GroundItemAmmo':
                return new GroundItemAmmo();
            case 'GroundItemWall':
                return new GroundItemWall();
        }
    }

    syncBullets(data, player) {
        player.gun.bullets = [];
        data.forEach(item => {
            let bullet = new Bullet;
            bullet.active = item.active;
            bullet.size = item.size;
            bullet.position = item.position;
            bullet.rotation = item.rotation;

            player.gun.bullets.push(bullet);
        });
    }

    addWall(data) {
        let wall = new Wall();
        wall.size = data.size;
        wall.position = data.position;

        this.walls.push(wall);
    }

    resetWalls() {
        this.walls = [];
    }
}
