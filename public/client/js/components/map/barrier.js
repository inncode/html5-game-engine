let Rect = require('../../../../shared/components/rect');

/**
 * TODO add sprite
 */
class Barrier extends Rect {
    constructor(x, y) {
        super(x, y);
    }

    draw(context, src) {
        context.save();

        let image = new Image();
        image.src = src;

        context.fillStyle = context.createPattern(image, 'repeat');
        context.fillRect(this.position.x, this.position.y, this.size.x, this.size.y);

        context.restore();
    }
}


module.exports = Barrier;