import Options from '../../../shared/options';

export class GameOverlay {
    constructor(canvas) {
        this._canvas = canvas;
        this._context = canvas.getContext('2d');
        this.grid = null;
    }

    drawRespawnTimerOverlay(respawnTime) {
        this._context.save();
        this._context.font = '60px Verdana';
        this._context.fillStyle = 'white';
        this._context.textAlign = 'center';
        this._context.textBaseline = 'middle';
        this._context.shadowColor = '#000';
        this._context.shadowOffsetX = 3;
        this._context.shadowOffsetY = 3;
        this._context.shadowBlur = 10;
        this._context.fillText('Respawning in: '+respawnTime, this._canvas.width / 2, this._canvas.height / 2);
        this._context.restore();
    }

    drawPlayerName(player) {
        if (player.dead) {
            return;
        }

        this._context.save();
        let text = player.username;
        if (text.length > 8) {
            text = text.substring(0, 8) + '...';
        }

        let blur = 10;
        this._context.font = "15px Verdana";
        this._context.textBaseline = 'middle';
        this._context.shadowColor = '#000';
        this._context.shadowOffsetX = 3;
        this._context.shadowOffsetY = 3;
        this._context.shadowBlur = blur;
        this._context.textAlign = "center";
        this._context.fillStyle = "white";
        this._context.fillText(text, player.position.x, player.position.y);
        this._context.restore();
    }

    drawPlayerInfo(player) {
        let text = 'Score: ' + player.score.points + ' | Kills: ' + player.score.kills + ' | Deaths: ' + player.score.deaths;

        this._context.save();

        this._context.font = '15px Verdana';
        this._context.textBaseline = 'middle';
        this._context.shadowColor = '#000';
        this._context.shadowOffsetX = 3;
        this._context.shadowOffsetY = 3;
        this._context.shadowBlur = 10;
        this._context.textAlign = "start";
        this._context.fillStyle = "white";
        this._context.fillText(text, 20, 20);

        this._context.restore();

        this.drawPlayerStatus(player);
    }

    drawPlayerStatus(player) {
        let text = 'Ammo: ' + player.ammo + ' | Blocks: ' + player.brick;

        this._context.save();

        this._context.font = '15px Verdana';
        this._context.textBaseline = 'middle';
        this._context.shadowColor = '#000';
        this._context.shadowOffsetX = 3;
        this._context.shadowOffsetY = 3;
        this._context.shadowBlur = 10;
        this._context.textAlign = "end";
        this._context.fillStyle = "white";
        this._context.fillText(text, this._canvas.width - 20, 20);

        this._context.restore();
    }

    drawGrid() {
        if (this.grid) {
            this._context.drawImage(this.grid, 0, 0);
            return;
        }

        let data = '\
        <svg width="100%" height="100%" xmlns="http://www.w3.org/2000/svg"> \
            <defs> \
                <pattern id="grid" width="'+Options.getCanvas().gridSize+'" height="'+Options.getCanvas().gridSize+'" patternUnits="userSpaceOnUse"> \
                    <path d="M '+Options.getCanvas().gridSize+' 0 L 0 0 0 '+Options.getCanvas().gridSize+'" fill="none" stroke="gray" stroke-width="1" /> \
                </pattern> \
            </defs> \
            <rect width="100%" height="100%" fill="url(#grid)" /> \
        </svg>';

        let DOMURL = window.URL || window.webkitURL || window;

        let img = new Image();
        let svg = new Blob([data], {type: 'image/svg+xml;charset=utf-8'});
        let url = DOMURL.createObjectURL(svg);

        let self = this;
        img.onload = function () {
            self.grid = img;
            self._context.drawImage(img, 0, 0);
            DOMURL.revokeObjectURL(url);
        };
        img.src = url;
    }

    drawPerformanceStats(latency, fps) {
        let text = 'Latency: ' + latency + ' ms | FPS: '+fps;

        this._context.save();

        this._context.font = '15px Verdana';
        this._context.textBaseline = 'middle';
        this._context.shadowColor = '#000';
        this._context.shadowOffsetX = 3;
        this._context.shadowOffsetY = 3;
        this._context.shadowBlur = 10;
        this._context.textAlign = "start";
        this._context.fillStyle = "white";
        this._context.fillText(text, 20, this._canvas.height - 20);

        this._context.restore();
    }
}