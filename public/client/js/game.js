import {GameManager} from './game-manager';

const canvas = document.querySelector('#game-area');
const gameManager = new GameManager(canvas);

gameManager.start();

let keys = {};
document.body.addEventListener('keydown', function (e) {
    console.log(e.keyCode);
    keys[e.keyCode] = true;
});
document.body.addEventListener('keyup', function (e) {
    delete keys[e.keyCode];
});

//Connecting To socket.io
let socket = io.connect(window.location.host);

let gameDiv = document.getElementById('game-div');
let preGameDiv = document.getElementById('pre-game-div');
let signDiv = document.getElementById('sign-div');
let errorDiv = document.getElementById('error-div');
let signDivUsername = document.getElementById('username');
let signDivPassword = document.getElementById('password');
let loginBtn = document.getElementById('login-btn');
let registerBtn = document.getElementById('register-btn');
let enterGameBtn = document.getElementById('enter-game-btn');
let pingStartTime;

loginBtn.onclick = function() {
    socket.emit('login', { 'username': signDivUsername.value, 'password': signDivPassword.value });
};
registerBtn.onclick = function() {
    socket.emit('register', { 'username': signDivUsername.value, 'password': signDivPassword.value });
};
enterGameBtn.onclick = function() {
    serverConnected(socket, socket.loggedUserData);
};

socket.on('login_register_response', function (data) {
    if (data.status) {
        signDiv.style.display = 'none';
        preGameDiv.style.display = 'block';

        socket.loggedUserData = data;
        socket.emit('highscores');
        $('#pregame-username').html(data.data.username);
    } else {
        errorDiv.style.display = 'block';
        errorDiv.innerHTML = data.message;
    }
});
socket.on('highscores-response', function (data) {
    data.forEach(function (item, key) {
        $('.highscores table tbody').append('<tr><td>'+(key+1)+'</td><td>'+item.username+'</td><td>'+item.points+'</td></tr>');
    });
});

socket.on('disconnect', function() {
    //Setting Message On Disconnection
    $('#sign-div').hide();
    $('#pre-game-div').hide();
    $('#game-div').hide();

    $('#connection-status-div').show();
    $('#connection-error-div').html("Disconnected from server, refresh your browser!").show();
});

socket.on('state', function(players) {
    if (!players) {
        return;
    }
    //Update players
    Object.entries(players).forEach(function (data) {
        gameManager.updatePlayer(data[0], data[1]);
    });

    //Deletes disconnected players
    Object.entries(gameManager.players).forEach(function (data) {
        if (!players[data[0]]) {
            gameManager.removePlayer(data[0]);
        }
    });
});

socket.on('sync_obstacles', function(obstacles) {
    if (!obstacles) {
        return;
    }

    gameManager.resetObstacles();
    obstacles.forEach(gameManager.syncObstacles);
});

socket.on('sync_ground_items', function (items) {
    if (!items) {
        return
    }

    gameManager.syncGroundItems(items);
});

socket.on('update-walls', function(walls) {
    if (!walls) {
        return;
    }

    gameManager.resetWalls();
    Object.entries(walls).forEach(function (data) {
        data[1].forEach(gameManager.addWall);
    });
});

socket.on('latency-client', function() {
    gameManager.latency = Date.now() - pingStartTime;
});

function serverConnected(socket, data) {
    preGameDiv.style.display = 'none';
    gameDiv.style.display = 'block';

    socket.emit('new_player', data);

    gameManager.localSocketId = socket.id;

    setInterval(function() {
        socket.emit('input', keys);
    }, 1000 / 60);

    setInterval(function() {
        pingStartTime = Date.now();
        socket.emit('latency-server');
    }, 2000);
}