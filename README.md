# Html5 game engine

##Installation

First, clone this repository:
```
$ git clone https://bitbucket.org/inncode/html5-game-engine.git
```

Next, copy .env.dist file
```
$ cp docker/.env.dist docker/.env
$ edit public/shared/options.js database settings
```

Then, run:
```
$ cd docker/
$ docker-compose up -d --build
$ docker-compose exec php yarn install
$ docker-compose exec php yarn build
$ docker-compose exec php node public/server/mysql/create-database.js
$ docker-compose exec php npm start
```

You are done! You can access main game engine page on the following URL: `http://localhost:8080/`