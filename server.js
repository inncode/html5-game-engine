let express = require('express');
let app = express();
let fs = require('fs');
let ServerManager = require('./public/server/server-manager');
let Socket = require('./public/server/network/socket');
let GameLoop = require('./public/server/network/game-loop');
let User = require('./public/server/mysql/user');

const port = 8080;

app.get('/', function (request, response) {
    response.writeHead(200, {"Content-Type": "text/html"});
    response.write(fs.readFileSync("./public/index.html"));
    response.end();
});

//Routing To Public Folder For Any Static Context
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/public/dist'));

console.log("Server Running At: localhost:" + port);

(new User()).logOutAll();
let httpServer = app.listen(port);
let serverManager = new ServerManager();
let socket = new Socket(httpServer, serverManager);
let gameLoop = new GameLoop(socket);

gameLoop.run();
